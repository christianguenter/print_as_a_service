CLASS zcx_print_as_a_service DEFINITION
  PUBLIC
  INHERITING FROM cx_static_check
  CREATE PUBLIC .

  PUBLIC SECTION.
    CLASS-METHODS:
      raise
        IMPORTING
          ix_previous TYPE REF TO cx_root
        RAISING
          zcx_print_as_a_service,

      raise_syst
        RAISING
          zcx_print_as_a_service,

      raise_text
        IMPORTING
          iv_text TYPE string
        RAISING
          zcx_print_as_a_service.

    METHODS:
      constructor
        IMPORTING
          textid   LIKE textid OPTIONAL
          previous LIKE previous OPTIONAL
          msg      TYPE symsg OPTIONAL
          text     TYPE string OPTIONAL,

      get_text REDEFINITION.

  PRIVATE SECTION.
    DATA:
      m_msg  TYPE symsg,
      m_text TYPE string.

    METHODS:
      get_msg_text
        RETURNING
          VALUE(rv_text) TYPE string.

ENDCLASS.



CLASS zcx_print_as_a_service IMPLEMENTATION.

  METHOD constructor ##ADT_SUPPRESS_GENERATION.

    super->constructor( textid = textid
                        previous = previous ).

    m_msg = msg.
    m_text = text.

  ENDMETHOD.

  METHOD raise.

    RAISE EXCEPTION TYPE zcx_print_as_a_service
      EXPORTING
        previous = ix_previous.

  ENDMETHOD.

  METHOD get_text.

    result = COND #( WHEN previous IS BOUND THEN previous->get_text( )
                     WHEN m_msg IS NOT INITIAL THEN get_msg_text( )
                     WHEN m_text IS NOT INITIAL THEN m_text
                     ELSE super->get_text( ) ).

  ENDMETHOD.

  METHOD raise_syst.

    RAISE EXCEPTION TYPE zcx_print_as_a_service
      EXPORTING
        msg = VALUE symsg( msgty = sy-msgty
                           msgid = sy-msgid
                           msgno = sy-msgno
                           msgv1 = sy-msgv1
                           msgv2 = sy-msgv2
                           msgv3 = sy-msgv3
                           msgv4 = sy-msgv4 ).

  ENDMETHOD.

  METHOD get_msg_text.

    MESSAGE ID m_msg-msgid
            TYPE m_msg-msgty
            NUMBER m_msg-msgno
            WITH m_msg-msgv1 m_msg-msgv2 m_msg-msgv3 m_msg-msgv4
            INTO rv_text.

  ENDMETHOD.

  METHOD raise_text.

    RAISE EXCEPTION TYPE zcx_print_as_a_service
      EXPORTING
        text = iv_text.

  ENDMETHOD.

ENDCLASS.
