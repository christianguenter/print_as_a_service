CLASS zcl_print_as_service DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.
    CLASS-METHODS:
      run
        IMPORTING
          iv_text    TYPE string
        EXPORTING
          ev_xstring TYPE xstring
          ev_mime    TYPE string
        RAISING
          zcx_print_as_a_service.

ENDCLASS.




CLASS zcl_print_as_service IMPLEMENTATION.

  METHOD run.

    DATA: lv_funcname     TYPE funcname,
          ls_docparams    TYPE sfpdocparams,
          ls_formoutput   TYPE fpformoutput,
          ls_outputparams TYPE sfpoutputparams,
          ls_result       TYPE sfpjoboutput.

    CLEAR: ev_xstring,
           ev_mime.

    TRY.
        CALL FUNCTION 'FP_FUNCTION_MODULE_NAME'
          EXPORTING
            i_name     = 'Z_PRINT_AS_A_SERVICE'
          IMPORTING
            e_funcname = lv_funcname.

      CATCH cx_fp_api INTO DATA(lx_error).
        zcx_print_as_a_service=>raise( ix_previous = lx_error ).
    ENDTRY.

    ls_outputparams-dest     = 'PDF'.
    ls_outputparams-nodialog = abap_true.
    ls_outputparams-getpdf   = abap_true.

    CALL FUNCTION 'FP_JOB_OPEN'
      CHANGING
        ie_outputparams = ls_outputparams
      EXCEPTIONS
        OTHERS          = 5.

    IF sy-subrc <> 0.
      zcx_print_as_a_service=>raise_syst( ).
    ENDIF.

    CALL FUNCTION lv_funcname
      EXPORTING
        /1bcdwb/docparams  = ls_docparams
        i_text             = iv_text
      IMPORTING
        /1bcdwb/formoutput = ls_formoutput
      EXCEPTIONS
        usage_error        = 1
        system_error       = 2
        internal_error     = 3
        OTHERS             = 4.

    IF sy-subrc <> 0.
      zcx_print_as_a_service=>raise_syst( ).
    ENDIF.

    CALL FUNCTION 'FP_JOB_CLOSE'
      IMPORTING
        e_result = ls_result
      EXCEPTIONS
        OTHERS   = 4.

    IF sy-subrc <> 0.
      zcx_print_as_a_service=>raise_syst( ).
    ENDIF.

    ev_xstring = ls_formoutput-pdf.
    ev_mime    = 'application/pdf'.

  ENDMETHOD.

ENDCLASS.
