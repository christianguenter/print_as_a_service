*&---------------------------------------------------------------------*
*& Report z_print_as_a_service_client
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_print_as_a_service_client.

PARAMETERS: text TYPE string LOWER CASE OBLIGATORY.

CLASS controller DEFINITION.

  PUBLIC SECTION.
    METHODS:
      start
        RAISING
          zcx_print_as_a_service.

  PRIVATE SECTION.
    METHODS:
      download
        IMPORTING
          iv_xstring TYPE xstring
        RAISING
          zcx_print_as_a_service,

      get_url
        RETURNING
          VALUE(rv_url) TYPE string.

ENDCLASS.

CLASS controller IMPLEMENTATION.

  METHOD start.

    cl_http_client=>create_by_url(
      EXPORTING
        url    = get_url( )
      IMPORTING
        client = DATA(li_client)
      EXCEPTIONS
        OTHERS = 4 ).

    IF sy-subrc <> 0.
      zcx_print_as_a_service=>raise_text( |Error { sy-subrc } from CREATE_BY_URL| ).
    ENDIF.

    li_client->request->set_form_field( name  = |text|
                                        value = text ).

    li_client->request->set_form_field( name  = |sap-client|
                                        value = |{ sy-mandt }| ).

    li_client->send(
      EXCEPTIONS
        http_communication_failure = 1
        http_invalid_state         = 2
        http_processing_failed     = 3
        http_invalid_timeout       = 4
        OTHERS                     = 5 ).

    IF sy-subrc <> 0.
      zcx_print_as_a_service=>raise_text( |Error { sy-subrc } from SEND| ).
    ENDIF.

    li_client->receive(
      EXCEPTIONS
        http_communication_failure = 1
        http_invalid_state         = 2
        http_processing_failed     = 3
        OTHERS                     = 4 ).

    IF sy-subrc <> 0.

      li_client->get_last_error(
        IMPORTING
          code    = DATA(code)
          message = DATA(message) ).

      cl_demo_output=>write( code ).
      cl_demo_output=>write( message ).
      cl_demo_output=>display(  ).
      RETURN.

    ENDIF.

    download( li_client->response->get_data( ) ).

  ENDMETHOD.


  METHOD download.

    CONSTANTS co_filename TYPE string VALUE 'C:\temp\test1234.pdf' ##NO_TEXT.

    CHECK iv_xstring IS NOT INITIAL.

    DATA(lt_data) = cl_bcs_convert=>xstring_to_solix( iv_xstring ).

    cl_gui_frontend_services=>gui_download(
      EXPORTING
        filename = co_filename
        filetype = 'BIN'
      CHANGING
        data_tab = lt_data
      EXCEPTIONS
        OTHERS   = 1 ).

    IF sy-subrc <> 0.
      zcx_print_as_a_service=>raise_text( |Error { sy-subrc } from GUI_DOWNLOAD| ).
    ENDIF.

    cl_gui_frontend_services=>execute(
      EXPORTING
        document = co_filename
      EXCEPTIONS
        OTHERS	 = 10 ).

    IF sy-subrc <> 0.
      zcx_print_as_a_service=>raise_text( |Error { sy-subrc } from EXECUTE| ).
    ENDIF.

  ENDMETHOD.


  METHOD get_url.

    cl_http_server=>get_location(
      EXPORTING
        protocol = 'http'
      IMPORTING
        host     = DATA(host)
        port     = DATA(port)
      RECEIVING
        url_part = DATA(url_part) ).

    rv_url =  |{ url_part }/sap/z_paas|.

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  TRY.
      NEW controller( )->start( ).
    CATCH zcx_print_as_a_service INTO DATA(lx_error).
      MESSAGE lx_error TYPE 'S' DISPLAY LIKE 'E'.
  ENDTRY.
