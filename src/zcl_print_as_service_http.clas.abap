CLASS zcl_print_as_service_http DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC.

  PUBLIC SECTION.
    INTERFACES:
      if_http_extension.

ENDCLASS.



CLASS zcl_print_as_service_http IMPLEMENTATION.

  METHOD if_http_extension~handle_request.

    DATA(lv_text) = server->request->get_form_field( |text| ).

    TRY.
        zcl_print_as_service=>run(
          EXPORTING
            iv_text    = lv_text
          IMPORTING
            ev_xstring = DATA(xstring)
            ev_mime    = DATA(mime) ).

        server->response->set_data( xstring ).
        server->response->set_content_type( mime ).
        server->response->set_status( code   = 200
                                      reason = |ok| ).

      CATCH zcx_print_as_a_service INTO DATA(lx_error).
        server->response->set_cdata( lx_error->get_text( ) ).
        server->response->set_status( code   = 400
                                      reason = |ok| ).
        RETURN.
    ENDTRY.

  ENDMETHOD.

ENDCLASS.
