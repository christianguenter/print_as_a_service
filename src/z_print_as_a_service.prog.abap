*&---------------------------------------------------------------------*
*& Report z_print_as_a_service
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_print_as_a_service.

PARAMETERS: text TYPE string LOWER CASE OBLIGATORY.

CLASS controller DEFINITION.

  PUBLIC SECTION.
    METHODS: start.

ENDCLASS.

CLASS controller IMPLEMENTATION.

  METHOD start.

    CONSTANTS co_filename TYPE string VALUE 'C:\temp\test1234.pdf' ##NO_TEXT.

    TRY.
        zcl_print_as_service=>run(
          EXPORTING
            iv_text    = text
          IMPORTING
            ev_xstring = DATA(xstring)
            ev_mime    = DATA(mime) ).

      CATCH zcx_print_as_a_service INTO DATA(lx_error).
        MESSAGE lx_error TYPE 'S' DISPLAY LIKE 'E'.
        RETURN.
    ENDTRY.

    DATA(lt_data) = cl_bcs_convert=>xstring_to_solix( xstring ).

    cl_gui_frontend_services=>gui_download(
      EXPORTING
        filename = co_filename
        filetype = 'BIN'
      CHANGING
        data_tab = lt_data
      EXCEPTIONS
        OTHERS   = 1 ).

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid
              TYPE 'S'
              NUMBER sy-msgno
              DISPLAY LIKE sy-msgty
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      RETURN.
    ENDIF.

    cl_gui_frontend_services=>execute(
      EXPORTING
        document               = co_filename
      EXCEPTIONS
        OTHERS                 = 10 ).

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid
              TYPE 'S'
              NUMBER sy-msgno
              DISPLAY LIKE sy-msgty
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      RETURN.
    ENDIF.

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  NEW controller( )->start( ).
