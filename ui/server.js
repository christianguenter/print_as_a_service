(function() {
  "use strict";

  // const fs = require("fs");
  // const writeFile = data => {
  //   const fileName = "c:\\temp\\test_node.pdf";
  //   fs.writeFile(fileName, data, { encoding: "binary" }, err => {
  //     if (err) {
  //       console.log("error: " + err.message);
  //       return;
  //     }

  //     console.log("The file " + fileName + " was saved");
  //   });
  // };

  const http = require("http");
  const path = require("path");
  const bodyParser = require('body-parser');

  const getPDF = (text, response) => {
    http
      .get(
        {
          hostname: "asgdbhd5.intranet.erpsourcing.ch",
          port: "8001",
          path: "/sap/z_paas?sap-client=500&text=" + escape(text)
        },
        (res) => {
          res.setEncoding("binary");
          let data = "";

          res.on("data", chunk => {
            data += chunk;
          });

          res.on("end", () => {
            response.type('pdf').send(new Buffer(data,'binary'));
          });
        }
      )
      .on("error", err => {
        console.log("error: " + err.message);
      });
  };

  const express = require("express");
  const app = express();

  app.use(express.json());
  app.use(express.urlencoded({
    extended: true
  }));

  app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
  });

  app.post("/printPdf", function(req, res) {;
    console.log(req.body);
    getPDF(req.body.text, res);
  });

  app.listen(3000, function() {
    console.log("Example app listening on port 3000!");
  });
})();
